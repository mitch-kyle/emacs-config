(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values
   '((eval add-hook 'org-babel-post-tangle
           (lambda nil
             (indent-region
              (point-min)
              (point-max))))
     (eval add-hook 'before-save-hook 'mkyle/org-todo-sort 1 t)
     (eval add-hook 'after-save-hook
           (lambda nil
             (dolist
                 (file
                  (org-babel-tangle-file
                   (buffer-file-name)
                   nil "emacs-lisp"))
               (byte-compile-file file)))
           1 t))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
